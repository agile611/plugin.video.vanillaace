Vanilla Ace
===

Vanilla Ace is a Acestream Live TV Addon for XBMC/Kodi.
It also contains an event agenda updated every 6 hours.
It needs Plexus installed and AceStream engine and player.